# LSD-SLAM docker
Docker setup for [lsd_slam_ros](https://gitlab.com/apl-ocean-engineering/lsd_slam_ros). Requires [wstool](http://wiki.ros.org/wstool).  

These instructions will first clone lsd_slam_ros into the src/ directory, before building it and all it's dependencies in the Docker image. The docker compose file adds the params/ and launch/ directories as voulmes in the image, which allows for editing of paramater and launch files outside the image. Log files are output to ~/.ros.  

**TODO**: X-term support is in development. To view image in lsd-slam though, run xhost local:root before compose. Still buggy  


## Setup
```
$ git clone https://gitlab.com/apl-ocean-engineering/lsd_slam_ros_docker.git
$ cd <lsd_slam_ros_docker>
$ wstool init src src/lsdslam.rosinstall
```

## Docker build and run
Build the docker file:
```
$ docker build -t lsd_slam_ros -f Dockerfile.wstool .  
```

That may take up to 10 minutes to run!  

Run the docker image:

```
$ docker-compose up
```

Note that this docker-compose file set volumes to the launch/ and params/ folders of lsd-slam, so those can be configured for run time adjustments  

On a host terminal run:

```
$ rostopic list
```

To verify the code is running correctly.
